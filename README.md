# Move Handler Frida
A version of [Move Handler](https://gitlab.com/Eagle517/move-handler) as a Frida agent. This is largely a test for a different standard of "DLL mods" for Blockland.

## Usage
Refer to [Move Handler](https://gitlab.com/Eagle517/move-handler) for in-game usage.

Assuming you have Node.js and the [Frida CLI tools installed](https://frida.re/docs/installation), you can build the source and get the agent loaded into Blockland via:
```
npm install
npm run build
frida -n "Blockland*" -l _agent.js
```
Ideally I will make a loader for Frida agents similar to Blockland Loader if I continue down this route.
