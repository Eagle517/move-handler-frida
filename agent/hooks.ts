const baseModule = Process.enumerateModules()[0];

export function scanAddr(signature: string): MemoryScanMatch[]
{
	const results = Memory.scanSync(baseModule.base, baseModule.size, signature);
	if (results.length == 0) {
		throw new Error("Unable to find address with signature: " + signature);
	}
	else {
		return results;
	}
}

export function scanFunc(signature: string): NativePointer
{
	const results = scanAddr(signature);
	if (results.length > 1) {
		throw new Error("Multiple addresses found for signature " + signature);
	}
	else {
		return results[0]["address"];
	}
}

export function defineFunc(lookup: string | NativePointer,
    retType: NativeType,
    argTypes: NativeType[],
    abiOrOptions?: NativeABI | NativeFunctionOptions): NativeFunction
{
	const addr = (typeof lookup == "string") ? scanFunc(lookup) : lookup;
	return new NativeFunction(addr, retType, argTypes, abiOrOptions);
}
