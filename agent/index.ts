import { defineFunc, scanFunc } from "./hooks";

const console__executefSig = "81 EC ?? ?? ?? ?? A1 ?? ?? ?? ?? 33 C4 89 84 24 ?? ?? ?? ?? 53 55 56 8B B4 24 ?? ?? ?? ?? 33 C9";
const onPlayerProcessTickStr = Memory.allocUtf8String("onPlayerProcessTick");
const onGetAIMoveStr = Memory.allocUtf8String("onGetAIMove");

const callOnPlayerProcessTick = defineFunc(console__executefSig, "pointer", ["int", "pointer", "...", "pointer", "pointer"]);
const callOnGetAIMove = defineFunc(console__executefSig, "pointer", ["int", "pointer", "...", "pointer"]);

const maxTriggerKeys = 5;

/**
 * Returns a string of a Move's real values in the format of
 * "x y z yaw pitch roll flags" in which flags is a 6 bit mask of
 * trigger[4] trigger[3] trigger[2] trigger[1] trigger[0] freeLook
 */
function moveToString(move: NativePointer): string
{
	if (move.isNull()) {
		return "0";
	}

	const moveString = move.add(24).readFloat() + " "  //x
	                 + move.add(28).readFloat() + " "  //y
	                 + move.add(32).readFloat() + " "  //z
	                 + move.add(36).readFloat() + " "  //yaw
	                 + move.add(40).readFloat() + " "  //pitch
	                 + move.add(44).readFloat() + " "; //roll

	let flags = move.add(56).readU8() ? 1 : 0;
	for (let i = 0; i < maxTriggerKeys; ++i) {
		if (move.add(57 + i).readU8() != 0) {
			flags |= 1 << (i + 1);
		}
	}

	return moveString + flags;
}

/**
 * Writes the data from the move string into the Move
 */
function stringToMove(moveString: string, move: NativePointer): void
{
	const data = moveString.split(" ");
	for (let i = 0; i < 6; ++i) {
		move.add(24 + (i*4)).writeFloat(parseFloat(data[i]));
	}

	const flags = parseInt(data[6]);
	const count = maxTriggerKeys + 1; //triggers and free look
	for (let i = 0; i < count; ++i) {
		move.add(56 + i).writeU8(((flags & (1 << i)) != 0) ? 1 : 0);
	}
}

//Player::processTick(const Move *move)
Interceptor.attach(scanFunc("55 8B EC 83 E4 F8 81 EC ?? ?? ?? ?? A1 ?? ?? ?? ?? 33 C4 89 84 24 ?? ?? ?? ?? 56 8B F1 57 8B 7D 08 89 74 24 18 89 7C 24 14"),
{
	onEnter(args) {
		if ("ecx" in this.context) {
			const player = this.context["ecx"];

			const isGhosted = (player.add(68).readU32() & 2) != 0;
			if (isGhosted) {
				return;
			}

			const id = Memory.allocUtf8String(player.add(32).readU32().toString());
			const moveString = Memory.allocUtf8String(moveToString(args[0]));
			callOnPlayerProcessTick(3, onPlayerProcessTickStr, id, moveString);
		}
	}
});

//AIPlayer::getAIMove(Move *move)
Interceptor.attach(scanFunc("55 8B EC 83 E4 F8 81 EC ?? ?? ?? ?? A1 ?? ?? ?? ?? 33 C4 89 84 24 ?? ?? ?? ?? 56 89 4C 24 44"),
{
	onEnter(args) {
		//ecx will always be present in this context,
		//but typescript doesn't know that
		if ("ecx" in this.context) {
			this.player = this.context["ecx"]; //ecx gets written to midway, must save
			this.move = args[0];
		}
	},

	onLeave(retval) {
		const player = this.player;

		const id = Memory.allocUtf8String(player.add(32).readU32().toString());
		const res = callOnGetAIMove(2, onGetAIMoveStr, id);
		const moveString = ptr(res.toString()).readCString();

		if (moveString != null && moveString != "") { //use TS return value
			if (moveString == "0") {
				retval.replace(ptr(0)); //0 -> use null move
				this.returnValue = 0;
			}
			else {
				stringToMove(moveString, this.move);
				retval.replace(ptr(1)); //1 -> use this move
			}
		}
		//else -> use original move
	}
});
